using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Temporizador : MonoBehaviour
{
    //Creamos el slider(temporizador), y un texto que va a marcar el tiempo que queda
    public Slider temporizador;
    public Text textoTiempo;
    public float tiempoJuego;
    //Para que cuando llegue a 00:00 se pare el temporizador
    private bool pararTiempo;
    public float tiempo;
    private float vidaNaruto;
    private float vidaDeku;
    public NexoNaruto nexoNaruto;
    public NexoDeku nexoDeku;
    

    void Start()
    {
        pararTiempo = false;
        temporizador.maxValue = tiempoJuego;
        temporizador.value = tiempoJuego;      
    }

   
    void Update()
    {      
        mostrarTiempo();
        cambiarEscenaGanador();
    }

    private void mostrarTiempo()
    {
        //Para que se vaya restando segundo a segundo
        tiempo = tiempoJuego - Time.timeSinceLevelLoad;

        //Minutos y segundos
        int minutos = Mathf.FloorToInt(tiempo / 60);
        int segundos = Mathf.FloorToInt(tiempo % 60);
        //Formato en el que queremos el tiempo
        string textTime = string.Format("{00:00}:{01:00}", minutos, segundos);


        if (pararTiempo == false)
        {
            textoTiempo.text = textTime;
            temporizador.value = tiempo;
        }
    }

    private void cambiarEscenaGanador()
    {
        vidaNaruto = FindObjectOfType<NexoNaruto>().BarraVida.value;
        vidaDeku = FindObjectOfType<NexoDeku>().BarraVida.value;

        //Si el tiempo llega a 0 se para
        if (tiempo < 1)
        {
            pararTiempo = true;
            if (vidaNaruto < vidaDeku)
            {
                SceneManager.LoadScene("GameOverDeku");
            }
            else
            {
                SceneManager.LoadScene("GameOverNaruto");
            }
        }

        //Gana Deku
        if (vidaNaruto < 1)
        {
            SceneManager.LoadScene("GameOverDeku");
        }

        //Gana Naruto
        if (vidaDeku < 1)
        {
            SceneManager.LoadScene("GameOverNaruto");
        }

        //Empate
        if (tiempo < 1 && vidaDeku == vidaNaruto)
        {
            SceneManager.LoadScene("GameOverEmpate");
        }

    }

}
