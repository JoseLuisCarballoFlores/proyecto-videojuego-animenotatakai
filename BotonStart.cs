using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Usamos esta librería para movernos entre escenas
using UnityEngine.SceneManagement;


public class BotonStart : MonoBehaviour
{
    public void botonStart()
    {
        //Le escribimos el nombre de la escena a la que queremos ir
        SceneManager.LoadScene("SampleScene");    
    }
    public void botonExit()
    {
        //Para cerrar la aplicación
        Application.Quit();
    }
    public void controles()
    {
        //Para ir a la escena de controles
        SceneManager.LoadScene("Controles");
    }

    public void info()
    { 
        //Para ir a la escena de información
        SceneManager.LoadScene("Información");
    }
}
