using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Funciones
{

    public static void darMaxValue(float tiempo, Slider slider1, Slider slider2, Slider slider3, Slider slider4)
    {
        slider1.maxValue = tiempo;
        slider2.maxValue = tiempo;
        slider3.maxValue = tiempo;
        slider4.maxValue = tiempo;
    }

    public static void darValue(float tiempo, Slider slider1, Slider slider2, Slider slider3, Slider slider4)
    {
        slider1.value = tiempo;
        slider2.value = tiempo;
        slider3.value = tiempo;
        slider4.value = tiempo;
    }

}
