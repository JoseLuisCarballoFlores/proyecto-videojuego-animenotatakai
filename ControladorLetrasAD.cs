using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorLetrasAD : MonoBehaviour
{
    //Creamos un array de objetos "botones"
    public Botones[] botones;

    //Para marcar la posición en la que nos encontramos
    public int posicion = 0;


    void Update()
    {

        pulsarLetras();

    }

    public void pulsarLetras()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            //El botón anterior se deselecciona
            botones[posicion].seleccionado = false;

            //Pasar a la siguiente posición
            posicion++;

            //Para que no se salga del array y pase al siguiente por el otro lado
            if (posicion < 0)
            {
                posicion = botones.Length - 1;
                botones[posicion].seleccionado = true;
            }
            if (posicion > botones.Length - 1)
            {
                posicion = 0;
                botones[posicion].seleccionado = true;
            }
            botones[posicion].seleccionado = true;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            //El botón anterior se deselecciona
            botones[posicion].seleccionado = false;

            //Pasar a la anterior posición
            posicion--;

            //Para que no se salga del array y pase al siguiente por el otro lado
            if (posicion < 0)
            {
                posicion = botones.Length - 1;
                botones[posicion].seleccionado = true;
            }
            if (posicion > botones.Length - 1)
            {
                posicion = 0;
                botones[posicion].seleccionado = true;
            }
            botones[posicion].seleccionado = true;
        }

    }
}
