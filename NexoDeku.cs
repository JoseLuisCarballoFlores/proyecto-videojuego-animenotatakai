using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Se utiliza esta librería
using UnityEngine.UI;

public class NexoDeku : MonoBehaviour
{
    
    public float vidas;
    public Slider BarraVida;

    //Si choca algún objeto con el nexo se baja vida 
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Gaara"))
        {
            float dañoGaara = other.collider.GetComponent<Gaara>().dañoTeamNaruto;
            vidas -= dañoGaara;
        }

        if (other.gameObject.CompareTag("Sasuke"))
        {
            float dañoSasuke = other.collider.GetComponent<Saske>().dañoTeamNaruto;
            vidas -= dañoSasuke;
        }

        if (other.gameObject.CompareTag("Kakashi"))
        {
            float dañoKakashi = other.collider.GetComponent<Kakashi>().dañoTeamNaruto;
            vidas -= dañoKakashi;
        }

        BarraVida.value = vidas;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Rasengan"))
        {
            vidas -= 5;
        }
        BarraVida.value = vidas;
    }
}
