using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deku : MonoBehaviour
{
    public float vidaTeamDeku;
    public Transform posicionDeku;
    public GameObject smash;
    public AudioSource sonido;


    void Start()
    {
        Instantiate(smash, posicionDeku);
        sonido.Play();
        StartCoroutine(muerte());
    }
    

    IEnumerator muerte()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }

}
