using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NexoNaruto : MonoBehaviour
{

    public float vidas;

    public Slider BarraVida;


    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("AllMight"))
        {
            float dañoAllMight = other.collider.GetComponent<AllMight>().dañoTeamDeku;
            vidas -= dañoAllMight;
        }

        if (other.gameObject.CompareTag("Bakugo"))
        {
            float dañoBakugo = other.collider.GetComponent<Bakugo>().dañoTeamDeku;
            vidas -= dañoBakugo;
        }

        if (other.gameObject.CompareTag("Tokoyami"))
        {
            float dañoTokoyami = other.collider.GetComponent<Tokoyami>().dañoTeamDeku;
            vidas -= dañoTokoyami;
        }

        BarraVida.value = vidas;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Smash"))
        {
            vidas -= 5;
        }
        BarraVida.value = vidas;
    }
}
