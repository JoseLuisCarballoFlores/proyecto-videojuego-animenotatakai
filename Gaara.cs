using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gaara : MonoBehaviour
{
    public float velocidad;
    public float vidaTeamNaruto;
    public float dañoTeamNaruto;
    private Animator animaciones;
    public float tiempoEntreAtaques;
    public float tiempoSiguienteAtaque;



    //Para inicializar variables al iniciar el programa
    private void Awake()
    {
        animaciones = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        movimiento();
        muerte();
        mejorarVelocidad();

        if (tiempoSiguienteAtaque > 0)
        {
            tiempoSiguienteAtaque -= Time.deltaTime;
        }
    }

    void movimiento()
    {
        gameObject.transform.Translate(velocidad * Time.deltaTime, 0, 0);//Eje x,y,z    
    }


    public void recibirDaño(float dañoEnemigo)
    {
        vidaTeamNaruto -= dañoEnemigo;
    }
    private void muerte()
    {
        if (vidaTeamNaruto <= 0)
            Destroy(gameObject);
    }


    //Para que cuando se choquen se cambie el animation a atacar
    void OnCollisionStay2D(Collision2D other)
    {
        
        if (other.gameObject.CompareTag("Tokoyami") && tiempoSiguienteAtaque <= 0)
        {
            animaciones.SetTrigger("Atacar");
            other.collider.GetComponent<Tokoyami>().recibirDaño(dañoTeamNaruto);
            tiempoSiguienteAtaque = tiempoEntreAtaques;
        }

        
        if (other.gameObject.CompareTag("AllMight") && tiempoSiguienteAtaque <= 0)
        {
            animaciones.SetTrigger("Atacar");
            other.collider.GetComponent<AllMight>().recibirDaño(dañoTeamNaruto);
            tiempoSiguienteAtaque = tiempoEntreAtaques;
        }

        if (other.gameObject.CompareTag("Bakugo") && tiempoSiguienteAtaque <= 0)
        {
            animaciones.SetTrigger("Atacar");
            other.collider.GetComponent<Bakugo>().recibirDaño(dañoTeamNaruto);
            tiempoSiguienteAtaque = tiempoEntreAtaques;
        }
        if (other.gameObject.CompareTag("NexoDeku"))
            Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Smash"))
        {
            Destroy(gameObject);
        }
    }

    void mejorarVelocidad()
    {
        float tiempoMejoras;
        tiempoMejoras = FindObjectOfType<Temporizador>().tiempo;
        if (tiempoMejoras < 20)
            velocidad += 0.002f;
    }
}
