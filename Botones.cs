using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//utilizamos esta librería
using UnityEngine.UI;

public class Botones : MonoBehaviour
{
    public Color[] colores;
    public Image botonImage;
    public bool seleccionado = false;

  
    void Start()
    {
        //Para que el botón empiece de ese color
        botonImage = GetComponent<Image>();
        //Para que no se salga del array(tiene 2 posiciones)
        botonImage.color = colores[1];
    }

    
    void Update()
    {
        //Si se selecciona ese botón, cambia de color
        cambiarColor();
        

    }

    public void cambiarColor()
    {
        if (seleccionado)
        {
            botonImage.color = colores[0];
        }
        else
        {
            botonImage.color = colores[1];
        }
    }


   
}
