using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Naruto : MonoBehaviour
{
    public float vidaTeamNaruto;
    public Transform posicionNaruto;
    public GameObject rasengan;
    public AudioSource sonido;


    void Start()
    {
        Instantiate(rasengan, posicionNaruto);
        sonido.Play();
        StartCoroutine(muerte());
    }

    IEnumerator muerte()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
}
