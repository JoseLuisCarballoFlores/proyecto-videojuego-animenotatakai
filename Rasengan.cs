using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rasengan : MonoBehaviour
{
    public float velocidad;

    void Update()
    {
        movimiento();
    }

    void movimiento()
    {
        gameObject.transform.Translate(velocidad * Time.deltaTime, 0, 0);//Eje x,y,z    
    }

    //Puesto en trigger para que no empuje a sus compañeros
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("NexoDeku"))
        {
            Destroy(gameObject);
        }
    }
}
