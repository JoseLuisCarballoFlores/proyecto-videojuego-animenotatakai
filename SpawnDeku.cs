using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnDeku : MonoBehaviour
{
    //Seleccionar personajes
    public List<GameObject> Personajes;
    //Seleccionar spawnspoints
    public List<Transform> spawnpoints;

    public float tiempoEntreSpawnsAllmight = 1;
    private float tiempoSiguienteSpawnAllmight;
    //Creamos el slider(temporizador)
    public Slider sliderAllMight;

    public float tiempoEntreSpawnsTokoyami = 2;
    private float tiempoSiguienteSpawnTokoyami;
    //Creamos el slider(temporizador)
    public Slider sliderTokoyami;

    public float tiempoEntreSpawnsBakugo = 3;
    private float tiempoSiguienteSpawnBakugo;
    //Creamos el slider(temporizador)
    public Slider sliderBakugo;

    public float tiempoEntreSpawnsDeku = 5;
    private float tiempoSiguienteSpawnDeku;
    //Creamos el slider(temporizador)
    public Slider sliderDeku;


    private void Update()
    {
        SpawnearPers();
        resetearTiempos();
    }

    void SpawnearPers()
    {
        if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) && tiempoSiguienteSpawnAllmight <= 0 && tiempoSiguienteSpawnBakugo <= 0 && tiempoSiguienteSpawnTokoyami <= 0 && tiempoSiguienteSpawnDeku <= 0)
        {
            //Instantanciamos el SpawnPersonajes
            SpawnPersonajes();
        }
    }

    void SpawnPersonajes()
    {
        int selPersojanes = FindObjectOfType<ControladorBotonesRightLeft>().posicion;
        int SelSpawns = FindObjectOfType<ControladorBotonesUpDown>().posicion;
        //Spawnea el personaje elegido, en la posicion que queramos
        GameObject spawnedPers = Instantiate(Personajes[selPersojanes], spawnpoints[SelSpawns]);

        if (spawnedPers.CompareTag("AllMight"))
        {
            tiempoSiguienteSpawnAllmight = tiempoEntreSpawnsAllmight;
            Funciones.darMaxValue(tiempoSiguienteSpawnAllmight, sliderAllMight, sliderTokoyami, sliderBakugo, sliderDeku);
        }

        if (spawnedPers.CompareTag("Bakugo"))
        {
            tiempoSiguienteSpawnBakugo = tiempoEntreSpawnsBakugo;
            Funciones.darMaxValue(tiempoSiguienteSpawnBakugo, sliderAllMight, sliderTokoyami, sliderBakugo, sliderDeku);
        }

        if (spawnedPers.CompareTag("Tokoyami"))
        {
            tiempoSiguienteSpawnTokoyami = tiempoEntreSpawnsTokoyami;
            Funciones.darMaxValue(tiempoSiguienteSpawnTokoyami, sliderAllMight, sliderTokoyami, sliderBakugo, sliderDeku);
        }

        if (spawnedPers.CompareTag("Deku"))
        {
            tiempoSiguienteSpawnDeku = tiempoEntreSpawnsDeku;
            Funciones.darMaxValue(tiempoSiguienteSpawnDeku, sliderAllMight, sliderTokoyami, sliderBakugo, sliderDeku);
        }

    }

    void resetearTiempos()
    {
        if (tiempoSiguienteSpawnAllmight > 0)
        {
            tiempoSiguienteSpawnAllmight -= Time.deltaTime;
            Funciones.darValue(tiempoSiguienteSpawnAllmight, sliderAllMight, sliderTokoyami, sliderBakugo, sliderDeku);
        }

        if (tiempoSiguienteSpawnBakugo > 0)
        {
            tiempoSiguienteSpawnBakugo -= Time.deltaTime;
            Funciones.darValue(tiempoSiguienteSpawnBakugo, sliderAllMight, sliderTokoyami, sliderBakugo, sliderDeku);
        }


        if (tiempoSiguienteSpawnTokoyami > 0)
        {
            tiempoSiguienteSpawnTokoyami -= Time.deltaTime;
            Funciones.darValue(tiempoSiguienteSpawnTokoyami, sliderAllMight, sliderTokoyami, sliderBakugo, sliderDeku);
        }


        if (tiempoSiguienteSpawnDeku > 0)
        {
            tiempoSiguienteSpawnDeku -= Time.deltaTime;
            Funciones.darValue(tiempoSiguienteSpawnDeku, sliderAllMight, sliderTokoyami, sliderBakugo, sliderDeku);
        }
    }

}
