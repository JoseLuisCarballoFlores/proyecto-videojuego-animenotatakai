using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnNaruto : MonoBehaviour
{
    //Seleccionar personajes
    public List<GameObject> Personajes;
    //Seleccionar spawnspoints
    public List<Transform> spawnpoints;

    public float tiempoEntreSpawnsSasuke = 1;
    private float tiempoSiguienteSpawnSasuke;
    //Creamos el slider(temporizador)
    public Slider sliderSasuke;

    public float tiempoEntreSpawnsGaara = 2;
    private float tiempoSiguienteSpawnGaara;
    //Creamos el slider(temporizador)
    public Slider sliderGaara;

    public float tiempoEntreSpawnsKakashi = 3;
    private float tiempoSiguienteSpawnKakashi;
    //Creamos el slider(temporizador)
    public Slider sliderKakashi;

    public float tiempoEntreSpawnsNaruto = 5;
    private float tiempoSiguienteSpawnNaruto;
    //Creamos el slider(temporizador)
    public Slider sliderNaruto;

    private void Update()
    {
        SpawnearPers();
        resetearTiempos();
    }

    void SpawnearPers()
    {
        if (Input.GetKeyDown(KeyCode.Space) && tiempoSiguienteSpawnSasuke <= 0 && tiempoSiguienteSpawnKakashi <= 0 && tiempoSiguienteSpawnGaara <= 0 && tiempoSiguienteSpawnNaruto <= 0)
        {
            //Instantanciamos el SpawnPersonajes
            SpawnPersonajes();
        }
    }

    void SpawnPersonajes()
    {

        int selPersonajes = FindObjectOfType<ControladorLetrasAD>().posicion;
        int SelSpawns = FindObjectOfType<ControladorLetrasWS>().posicion;
        //Spawnea el personaje elegido, en la posicion que queramos
        GameObject spawnedPers = Instantiate(Personajes[selPersonajes], spawnpoints[SelSpawns]);

        if (spawnedPers.CompareTag("Sasuke"))
        { 
            tiempoSiguienteSpawnSasuke = tiempoEntreSpawnsSasuke;
            Funciones.darMaxValue(tiempoSiguienteSpawnSasuke, sliderSasuke, sliderKakashi, sliderGaara, sliderNaruto);
        }

        if (spawnedPers.CompareTag("Kakashi"))
        {
            tiempoSiguienteSpawnKakashi = tiempoEntreSpawnsKakashi;
            Funciones.darMaxValue(tiempoSiguienteSpawnKakashi, sliderSasuke, sliderKakashi, sliderGaara, sliderNaruto);
        }

        if (spawnedPers.CompareTag("Gaara"))
        {
            tiempoSiguienteSpawnGaara = tiempoEntreSpawnsGaara;
            Funciones.darMaxValue(tiempoSiguienteSpawnGaara, sliderSasuke, sliderKakashi, sliderGaara, sliderNaruto);
        }

        if (spawnedPers.CompareTag("Naruto"))
        {
            tiempoSiguienteSpawnNaruto = tiempoEntreSpawnsNaruto;
            Funciones.darMaxValue(tiempoSiguienteSpawnNaruto, sliderSasuke, sliderKakashi, sliderGaara, sliderNaruto);
        }
    }

    void resetearTiempos()
    {
        if (tiempoSiguienteSpawnSasuke > 0)
        {
            tiempoSiguienteSpawnSasuke -= Time.deltaTime;
            Funciones.darValue(tiempoSiguienteSpawnSasuke, sliderSasuke, sliderKakashi, sliderGaara, sliderNaruto);
        }

        if (tiempoSiguienteSpawnKakashi > 0)
        {
            tiempoSiguienteSpawnKakashi -= Time.deltaTime;
            Funciones.darValue(tiempoSiguienteSpawnKakashi, sliderSasuke, sliderKakashi, sliderGaara, sliderNaruto);
        }
            

        if (tiempoSiguienteSpawnGaara > 0)
        {
            tiempoSiguienteSpawnGaara -= Time.deltaTime;
            Funciones.darValue(tiempoSiguienteSpawnGaara, sliderSasuke, sliderKakashi, sliderGaara, sliderNaruto);
        }
            

        if (tiempoSiguienteSpawnNaruto > 0)
        {
            tiempoSiguienteSpawnNaruto -= Time.deltaTime;
            Funciones.darValue(tiempoSiguienteSpawnNaruto, sliderSasuke, sliderKakashi, sliderGaara, sliderNaruto);
        }
           
    }


} 
