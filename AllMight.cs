using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AllMight : MonoBehaviour
{
    public float velocidad;
    public float vidaTeamDeku;
    public float dañoTeamDeku;
    private Animator animaciones;
    public float tiempoEntreAtaques;
    public float tiempoSiguienteAtaque;


    //Para inicializar variables al iniciar el programa
    private void Awake()
    {
        animaciones = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        movimiento();
        muerte();
        mejorarVelocidad();

        if (tiempoSiguienteAtaque > 0)
        {
            tiempoSiguienteAtaque -= Time.deltaTime;
        }
    }

    void movimiento()
    {  
       gameObject.transform.Translate(-velocidad * Time.deltaTime, 0, 0);//Eje x,y,z    
    }

    public void recibirDaño(float dañoEnemigo)
    {
        vidaTeamDeku -= dañoEnemigo;
    }

    private void muerte()
    {
        if (vidaTeamDeku <= 0)
            Destroy(gameObject);
    }


    //Para que cuando se choquen se cambie el animation a atacar
    void OnCollisionStay2D(Collision2D other)
    {
        
        if (other.gameObject.CompareTag("Sasuke") && tiempoSiguienteAtaque <= 0)
        {
            animaciones.SetTrigger("Atacar");
            other.collider.GetComponent<Saske>().recibirDaño(dañoTeamDeku);
            tiempoSiguienteAtaque = tiempoEntreAtaques;
        }

        if (other.gameObject.CompareTag("Gaara") && tiempoSiguienteAtaque <= 0)
        {
            animaciones.SetTrigger("Atacar");
            other.collider.GetComponent<Gaara>().recibirDaño(dañoTeamDeku);
            tiempoSiguienteAtaque = tiempoEntreAtaques;
        }

        if (other.gameObject.CompareTag("Kakashi") && tiempoSiguienteAtaque <= 0)
        {
            animaciones.SetTrigger("Atacar");
            other.collider.GetComponent<Kakashi>().recibirDaño(dañoTeamDeku);
            tiempoSiguienteAtaque = tiempoEntreAtaques;
        }

        if (other.gameObject.CompareTag("NexoNaruto"))
            Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Rasengan"))
        {
            Destroy(gameObject);
        }
    }

    void mejorarVelocidad()
    {
        float tiempoMejoras;
        tiempoMejoras = FindObjectOfType<Temporizador>().tiempo;
        if (tiempoMejoras < 20)
            velocidad += 0.003f;
    }
}
