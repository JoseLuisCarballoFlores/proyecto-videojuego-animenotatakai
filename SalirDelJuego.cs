using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SalirDelJuego : MonoBehaviour
{
    private int numVecesParaSalir = 0;

    void Update()
    {
        pulsarParaSalir();
    }

    void pulsarParaSalir()
    {
        if (Input.GetKeyDown(KeyCode.F1))
            numVecesParaSalir++;

        if(numVecesParaSalir==3)
        Application.Quit();
    }

}
